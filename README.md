# autumn-lobby
Starter with the bare essentials needed for a [Gatsby](https://www.gatsbyjs.org/) site

Install CLI tool:

```
npm install --global gatsby-cli
```

## Running in development
`gatsby develop`

## Running production build
`gatsby serve`

## Build files for deployment
`gatsby build`

## Information
- deployed with [surge](surge.sh)
- domain: unequaled-crook.surge.sh

## install surge before deploy
```
npm install --global surge
surge # creates free account
```

## Running deploy
```
npm run deploy
```

## Entry-points
- application main: 
    ```
    pages/index.js
    ```
- image assets dir: 
    ```
    assets/images/*
    ```
- internal html excluded from build path
    ```
    static/*
    ```