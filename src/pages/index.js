import React from "react"
/* components */
import ActionBar from '../components/ActionBar'
import Arcade from '../components/Arcade'
import Banner from '../components/Banner'
import Background from '../components/Background'
import Footer from '../components/Footer'
import Navigation from '../components/Navigation'
import Container from '../components/Container'
/* slots images */
import slot1 from '../assets/images/slots/slot1.png'
import slot2 from '../assets/images/slots/slot2.png'
import slot3 from '../assets/images/slots/slot3.png'
import slotheader from '../assets/images/slots/slotheader.png'
/* chance images */
import chance1 from '../assets/images/chance/chance1.png'
import chance2 from '../assets/images/chance/chance2.png'
import chance3 from '../assets/images/chance/chance3.png'
import chanceheader from '../assets/images/chance/chanceheader.png'
/* racing images */ 
import racing1 from '../assets/images/racing/racing1.png'
import racing2 from '../assets/images/racing/racing2.png'
import racing3 from '../assets/images/racing/racing3.png'
import racingheader from '../assets/images/racing/racingheader.png'
/* newgen images */
import newgen1 from '../assets/images/newgen/newgen1.png'
import newgen2 from '../assets/images/newgen/newgen2.png'
import newgen3 from '../assets/images/newgen/newgen3.png'
import newgen4 from '../assets/images/newgen/newgen4.png'
import newgenheader from '../assets/images/newgen/newgenheader.png'
/* etc */
import logo from '../assets/images/etc/logo.png'

import '../styles/global.css'

const slots = [
    { image: slot1, url: '/dino/index.html' }, 
    { image: slot2, url: '/oishi/index.html' }, 
    { image: slot3, url: '/pirates/index.html' }
]

const chance = [
    { image: chance1, url: '/pokerroulette/index.html' }, 
    { image: chance2, url: '/triplechance/index.html'}, 
    { image: chance3, url: '' }
]
const racing = [
    { image: racing1, url: '' }, 
    { image: racing2, url: '' }, 
    { image: racing3, url: '' }
]
const newgen = [
    { image: newgen1, url: '' }, 
    { image: newgen2, url: '' }, 
    { image: newgen3, url: '' }, 
    { image: newgen4, url: '' }
]

export default () => (
    <Background>
        <Navigation 
            logo={logo}
        />
        <Container>
            <Banner />
            <ActionBar />
            <Arcade 
                title={slotheader}
                images={slots}
            />
            <Arcade
                title={chanceheader}
                images={chance}
            />
            <Arcade
                title={racingheader}
                images={racing}
            />
            <Arcade
                title={newgenheader}
                images={newgen}
            />
        </Container>
        <Footer />
    </Background>
)