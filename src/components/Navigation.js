import React, { Component } from 'react'
import styles from '../styles/navigation.module.css'

export default class extends Component {
    render () {
        console.log(styles)
        return (
            <div className={styles.Nav}>
                <div className={styles.Wrapper}>
                    <img src={this.props.logo} alt="logo" />
                </div>
                <ul>
                    <li><a href="https://www.example.com" target="_blank">Sign up</a></li>
                    <li><a href="https://www.example.com" target="_blank">Login</a></li>
                </ul>
            </div>
        )
    }
}