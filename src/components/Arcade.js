import React, { Component } from 'react'
import { withPrefix } from 'gatsby-link'
import styles from '../styles/arcade.module.css'

export default class extends Component {
    state = { openCol: false }

    renderImages = arr => arr.map((e, i) => {
        return (
            <a href={withPrefix(e.url)} target="_blank" key={i}>
                <img className={styles.Images} src={e.image} alt="stuff" key={i} />
            </a>
        )
    })

    toggleShowHandler = () => {
        const openCol = !this.state.openCol
        this.setState(prev => ({ ...prev, openCol }))
    }

    renderCollapsible = hiddenImages => {
        const { openCol } = this.state
        return (
            <div className={styles.Collapsible}>
                <div className={
                    openCol
                        ? [styles.ActiveContent, styles.ImageContainer].join(" ")
                        : [styles.Content, styles.ImageContainer].join(" ")
                }>
                    {this.renderImages(hiddenImages)}
                </div>
                <button
                    className={
                        openCol
                            ? [styles.Button, styles.Showless].join(" ")
                            : [styles.Button, styles.Showmore].join(" ")
                    }
                    onClick={this.toggleShowHandler}
                />
            </div>
        )
    }

    render () {
        const { images } = this.props
        const displayImages = images.slice(0, 3)
        const hiddenImages = images.slice(3, images.length)
        return (
            <div className={styles.Arcade}>
                <div className={styles.Title}>
                    <img src={this.props.title} alt=""/>
                </div>
                <div className={styles.ImageContainer}>
                    {this.renderImages(displayImages)}
                </div>
                { hiddenImages.length > 0 
                  ? this.renderCollapsible(hiddenImages) 
                  : null }
            </div>
        )
    }
}