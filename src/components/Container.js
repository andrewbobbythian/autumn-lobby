import React, { Component } from 'react'
import styles from '../styles/container.module.css'

export default class extends Component {
    render () {
        return (
            <div className={styles.Container}>
                {this.props.children}
            </div>
        )
    }
}