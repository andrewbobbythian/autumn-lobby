import React, { Component } from 'react'
import styles from '../styles/background.module.css'

export default class extends Component {
    render () {
        return (
            <div className={styles.Background}>
                {this.props.children}
            </div>
        )
    }
}