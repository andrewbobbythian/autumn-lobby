import React, { Component } from 'react'
import styles from '../styles/actionbar.module.css'

export default class extends Component {
    render () {
        return (
            <div className={styles.ActionBar}>
                <div className={styles.SearchBar}></div>
                <div className={styles.ViewStyle}></div>
            </div>
        )
    }
}