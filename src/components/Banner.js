import React, { Component } from 'react'
import styles from '../styles/banner.module.css'
import banner from '../assets/images/etc/banner.png'

export default class extends Component {
    render () {
        return (
            <div>
                <img src={banner} alt="banner" className={styles.Banner} />
                <div className={styles.Marque}>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                </div>
            </div>
        )
    }
}